package snippets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Snippet {

	private static Snippet instance;
	private Timer timer;
	private Random  rndNumber = new Random();  

	private Date startDate = new Date();
 
	protected Snippet() {
		//Singleton - is/must be private
	}

	public static Snippet getInstance () {
		if (instance == null) {

			instance = new Snippet ();
		}
		return instance;
	}

	
	// Logs //
	
	public void log (String LogText) {
		System.out.printf("%n" + datetimeToString(new Date()) +" "+  LogText); 
	}
	public void log2 (String LogText) {
		System.out.printf("%n" +LogText); 
	}
	public void logMethod (String LogText) {
		System.out.printf("%n<" +Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + LogText+"/>"); 
	}
	
    public String getMethodName() {
       return Thread.currentThread().getStackTrace()[2].getMethodName() ;
        
    }

	public   void logErrors (String LogText) {
		System.out.printf("%n%s method:%s error:%s", datetimeToString(new Date()),Thread.currentThread().getStackTrace()[2] , LogText); 
 	}

	// dates //
	
	public String dateToyyyyMMddString   (Date myDate ) {
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		return (ft.format(myDate));
	}
	
	public  String datetimeToString (Date date) {
		SimpleDateFormat ft = new SimpleDateFormat ("yy.MM.dd HH:mm:ss.SSS");
		return (ft.format(date));
	} 
	
	public  String dateToHHMMString (Date date) {
		SimpleDateFormat ft = new SimpleDateFormat ("HH:mm");
		return (ft.format(date));
	} 


	public  String getTimeDiff (Date startDate, Date endDate) {
		return (endDate.getTime() - startDate.getTime()) / 1000.0 + " sec";
	}

	public  String getTimeLapse () {
		return (new Date().getTime() - this.startDate.getTime()) / 1000.0 + " sec";
	}
	public  double getTimeLapseInSeconds () {
		return (new Date().getTime() - this.startDate.getTime()) / 1000.0 ;
	}


	public String dateToString   (Date myDate ) {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return myDateFormat.format(myDate);
	}
	
	public Date stringyyyyMMddToDate (String s) {
		DateFormat formatter ; 
		Date date = null ;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = formatter.parse(s);
		} catch (ParseException e) {
		}
		return date;
	} 

	public Date getStartDate() {
		return startDate;
	}

	public void setStartTimer() {
		this.startDate = new Date();
	}

	public String getTimeStamp() {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("HH:mm");
		return myDateFormat.format(new Date());
	}
	public String getTimeStamp2() {
		SimpleDateFormat myDateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		return myDateFormat.format(new Date());
	}

	// strings //
	
	
	public String get1stHalf(String input, String limitor) {
		String command;
		int pos = input.indexOf(limitor);
		try {
			command = input.substring(0,pos) ;
		} 
		catch (Exception e) {
			command = input.trim();
		}
		return command; 
	}

	public String get2ndHalf(String input, String limitor) {
		String value = "";
		int pos = input.indexOf(limitor) +1;

		try {
			value = input.substring(pos).trim();
		} 
		catch (Exception e) {
			value = "";
		}
		return value; 
	}
	
	// system // 
	
	public  void shutDownSystem(int timeDelay) {
		try {
			Thread.sleep(timeDelay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}   
		finally {
			System.exit(0);
		}
	}
	
	public void timedMessage(final JLabel label, String message, int waitTime){
		label.setText(message);
		timer = new Timer(waitTime, new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				label.setText("");
			}
		});
		timer.setRepeats(false);
		timer.start();
	}
	
	public int getRandomInteger(int from, int to) {
		return rndNumber.nextInt(to-from) + from; 
	}
	
	public String popUpWindow ( String message, String title) {
		JFrame frame = new JFrame();
		return  JOptionPane.showInputDialog(frame, message, title,JOptionPane.PLAIN_MESSAGE);
	}
	
}

