package snippets;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author larsklingsten
 * @version 2014-10-07a
 */
public class InputOutput {
	private static InputOutput instance;

	private InputOutput() { 
		//nothing
	}

	public static InputOutput getInstance() {
		if (instance == null) {
			instance = new InputOutput();
		}
		return instance;

	}

	public Object loadXmlFromFile(Class myClass, String filename) throws JAXBException, FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(myClass);
		FileInputStream reader = new FileInputStream(filename);
		Unmarshaller u = context.createUnmarshaller();
		return u.unmarshal(reader);
	}

	public void saveXmlToFile(Object rootElement, String filename) throws JAXBException, FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(rootElement.getClass());
		FileOutputStream outputFile = new FileOutputStream(filename);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(rootElement, outputFile);
	}

	public XMLGregorianCalendar stringToXMLGregorianCalendar(String s) throws ParseException, DatatypeConfigurationException {
		XMLGregorianCalendar result = null;
		Date date;
		SimpleDateFormat simpleDateFormat;
		GregorianCalendar gregorianCalendar;

		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		date = simpleDateFormat.parse(s);
		gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
		gregorianCalendar.setTime(date);
		result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		return result;
	}

	public  String readFile(String path, Charset encoding)       {
		byte[] encoded;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
		} catch (IOException ex) {
			Snippet.getInstance().log("error:" + ex.getLocalizedMessage());
		}
		return "Failure"; 
	}
}
