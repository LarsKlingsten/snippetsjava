package snippets;

public class MyTimer {
	long startTime;

	public MyTimer () {
		startTime = System.nanoTime();
	}

	public double elapsed() {
		return (System.nanoTime()-startTime ) / 1000000000.0;  	
	}

	public String elapsedString() {
		return String.format("%.10f secs", elapsed());  	
	}
	
	public void wait (int delayInMiliSecs) {
		try {
			Thread.sleep(delayInMiliSecs);
		} catch (InterruptedException e) {
			 e.printStackTrace();
		}
	}
}