 
package snippets;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author larsklingsten
 */
public class Regex {
    private static Regex instance;
    
    private Regex() {
    }
    
    public static Regex getInstance() {
        if (instance == null) {
            instance = new Regex();
        }
        return instance;
    }
    
    public final Pattern StringInBrackets = Pattern.compile("<(.*)>", Pattern.CASE_INSENSITIVE);
    public String getStringsInBrackets (String StringsWithGarbage) {
        String result = "";
        Matcher matcher =  StringInBrackets .matcher(StringsWithGarbage);
        if ( matcher.find()) {
            result =  matcher.group(1);
        }
        return result;
    }
    
    public final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public boolean validate(String emailStr) {
        emailStr = emailStr.trim();
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        boolean result = matcher.find();
        return result;
    }
    
}
